<?php

use App\Http\Controllers\DdtApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => ['auth:sanctum']], function () {

    Route::get('/products', [ProductApiController::class, 'get']);

    #Route::post('/product', [ProductApiController::class, 'post']);
    #Route::post('/products-bulk', [ProductApiController::class, 'postBulk']);

    Route::post('/product/{ean}', [ProductApiController::class, 'put']);



    Route::post('/ddt/{code}',[DdtApiController::class,'get']);

});

Route::post('login', '\App\Http\Controllers\Auth\LoginController@apiLogin');
