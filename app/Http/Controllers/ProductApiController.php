<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Manufacturer;
use App\Models\Category;
use App\Models\Productgroup;
use App\Models\Dump;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class ProductApiController extends Controller
{
    //

    var $extIdMerchants = [245];

    public function get(Request $request)
    {
        #$request->validate([]);
        $user = Auth::user();
        $products = Auth::user()->stores->pluck('products')->flatten()->unique();
        return ['status' => 1, 'data' => $products];
    }

    #create new product

    public function postBulk(Request $request){

        ########################################################################

        # REQUIRES VALIDATION

        ########################################################################
        $user = Auth::user();
        Dump::create(['content' => $request->all(),'action' => 'apiPostBulk','user_id' => $user->id ]);

        $return = [];

        foreach($request->products as $product){

            $validation = $this->validateProductNode($product);

            if (!$validation['status']) {
                return response()->json($validation['messages'], 200);
            }

            $return[] = $this->createProduct($product);
        }

        return $return;

    }

    public function validateProductNode($product){
        $validator = Validator::make($product, [
            'group' => 'required',
            'lang' => [
                'required',
                Rule::in(['it', 'en']),
            ],
            'manufacturer' => 'required',
            'category' => 'required',
            'sku' => 'required',
            'ean' => 'required',
            'name' => 'required',
            'short_description' => 'required',
            #'description' => 'required',
            'width' => 'required|numeric',
            'height' => 'required|numeric',
            'depth' => 'required|numeric',
            'weight' => 'required|numeric',
            'width_with_package' => 'required|numeric',
            'height_with_package' => 'required|numeric',
            'depth_with_package' => 'required|numeric',
            'weight_with_package' => 'required|numeric',
            #'html_description' => 'required',
            'is_group_default' => [
                'required',
                Rule::in([1, 0])
            ],
            'availability_days' => 'required|numeric',
            'images' => [
                'required',
                'array'
            ],
            'quantity' => 'required|numeric',
            'sell_in' => 'required|numeric',
            'sell_out' => 'numeric',
            'warranty_years' => 'sometimes|required|numeric',
            'min_quantity' => 'sometimes|required|numeric',
            'returnable' =>
            [
                'sometimes',
                'required',
                Rule::in([1, 0])
            ],
            'handling_cost' => 'sometimes|required|numeric'
        ]);

        if ($validator->fails()) {
            return ['status' => false,'messages'=> $validator->messages()];
        }
        return ['status' => true];
    }

    public function post(Request $request)
    {
        $user = Auth::user();
        Dump::create([
            'content' => $request->all(),
            'action' => 'apiPost',
            'user_id' => $user->id
        ]);

        $validation = $this->validateProductNode($request->all());

        if(!$validation['status']){
            #return response()->json($validation['messages'], 200);
            return $validation['messages'];
        }

        return $this->createProduct($request->all());

    }

    public function createProduct($productData){

        $request = $productData;

        $lang = $productData['lang'];
        $manufacturer = Manufacturer::findByName($request['manufacturer']);
        $category = Category::findByName($request['category'], $lang);
        $productGroup = Productgroup::getByNameAndManufacturer($request['group'], $manufacturer->id);

        $user = Auth::user();

        $product = Product::where('manufacturer_id', $manufacturer->id)->where('barcode', $request['ean'])->first(); #check if product exists already

        if($product == null){
            $product = $user->stores->first()->products()->create([
                'enabled' => 0,
            ]);
        }else{
            return ['status' => 1, 'product' => $product,'message' => 'product already existing'];
        }

        $data = [
            'sku' => $request['sku'],
            'barcode' => $request['ean'],
            'manufacturer_id' => $manufacturer->id,
            'category_id' => $category->id,
            'name' => [
                $lang => $request['name']
            ],
            //'subtitle' => [
            //    $lang => $request->subtitle
            //],
            'short_description' => [
                $lang => $request['short_description']
            ],
            'description' => [
                $lang => $request['description']
            ],
            'html_description' => [
                $lang => $request['html_description']
            ],
            //'continue_selling_when_out_of_stock' => $request->no_stock_behavior,
            'width' => $request['width_with_package'],
            'height' => $request['height_with_package'],
            'depth' => $request['depth_with_package'],
            'weight' => $request['weight_with_package'],
            'inner_width' => $request['width'],
            'inner_height' => $request['height'],
            'inner_depth' => $request['depth'],
            'inner_weight' => $request['weight'],
            'productgroup_id' => $productGroup->id,
            'is_group_default' => $request['is_group_default'],
            'availability_days' => $request['availability_days'],
            'photos' => $request['images'],
            'taxregime_id' => 1, #by default 1 italian standard rate
            'quantity' => $request['quantity'],
            'price_sell_in' => $request['sell_in'],
            'price_sell_out_suggested' => $request['sell_out'],
            'warranty_years' => isset($request['warranty_years']) ? $request['warranty_years'] : 2,
            'min_quantity' => isset($request['min_quantity']) ? $request['min_quantity'] : 1,
            'returnable' => isset($request['returnable']) ? $request['returnable'] : 1,
            'extra_shipping_cost' => isset($request['handling_cost']) ? $request['handling_cost'] : 0
        ];

        $product->fill($data);
        $product->save();

        return ['status' => 1, 'product' => $product];

    }

    #update product
    #to update quantity pass attribute quantity
    public function put(Request $request, $ean)
    {

        $user = Auth::user();

        $dumpData = $request->all();
        $dumpData['ean'] = $ean;

        Dump::create([
            'content' => $dumpData,
            'action' => 'apiPut',
            'user_id' => $user->id
        ]);

        $validator = Validator::make($request->all(), [
            'field' => [
                Rule::in([
                    'quantity', 'price_sell_in','min_quantity'
                ])
            ],
            'value' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        }

        $store = $user->stores->first();

        if(in_array($user->id, $this->extIdMerchants)){
            if(!isset($dumpData['ext_id'])){
                return ['status' => 0, 'message' => 'ext_id param is missing'];
            }
            $sku = $dumpData['ext_id'];
            $product = $store->products()->where('sku',$sku)->first();
            if (is_null($product)) {
                return ['status' => 0, 'message' => 'There is no product with ext_id ' . $sku];
            }
            $product->barcode = $ean;

        }

        else{
            $product = $store->products()->where('barcode', $ean)->first();
            if(is_null($product)) {
                return ['status' => 0, 'message' => 'There is no product with ean ' . $ean];
            }
        }


        $product->fill([
            $request->field => $request->value
        ]);

        $product->save();

        return ['status' => 1, 'data' => $product];
    }

}




        #group: products belonging to the same group (variations of the same products ) must share this value
        #sku required
        #barcode required
        #lang, valori ammessi it o en required
        #manufacturer, nome del brand required
        #no_stock_behavior
        #width, height, depth in mm required
        #weight in gg required
        #così come i vari inner required
        #is_group_default è la versione che deve essere mostrata nei risultati di ricerca [1,0]
        #availability_days quanti giorni (solitamente) torna ad essere disponibile un prodotto quando è terminata la disponibilità, se non torna 0
        #images, array di immagini required
        #quantity, disponibilità al momento required
        #cost, prezzo a Jinnone required
        #warranty_years, anni [numero intero] di garanzia
        #min_quantity, quantità minima ordinabile d1
        #returnable, [1,0] il prodotto può essere reso d1
        #html_description
        #handling_cost, costo di gestione di ogni prodotto
