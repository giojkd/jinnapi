<?php

namespace App\Http\Controllers;

use App\Models\Ddt;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class DdtApiController extends Controller
{
    //
    public function get(Request $request, $code)
    {
        $user = Auth::user();
        $stores = $user->stores;
        $ddts = Ddt::whereIn('store_id',$stores->pluck('id'))->where('code',$code)->get();
        $ddts->transform(function($ddt,$key){
            return collect([
                'id' => $ddt->id,
                'created_at' => $ddt->created_at,
                'code' => $ddt->code,
                'notes' => $ddt->notes,
                'rows' => json_decode($ddt->rows,1)
            ]);
        });

        return [
            'status' => ($ddts->count() > 0) ? 1 : 0,
            'data' => $ddts->first()
        ];
    }
}
