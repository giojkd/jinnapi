<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    use HasFactory;
    public $guarded = [];
    public static function findByName($name){

        $manufacturer = Manufacturer::where('name',$name)->first();

        if(is_null($manufacturer)){
            $manufacturer = Manufacturer::create(['name' => $name,'enabled' => 0]);
        }

        return $manufacturer;
    }

}
