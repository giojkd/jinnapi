<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
class Productgroup extends Model
{
    use HasFactory;
    public $guarded = [];
    public static function getByNameAndManufacturer($name,$manufacturerId ){

        $group = Productgroup::where('manufacturer_id', $manufacturerId)->where('ext_id',$name)->first();

        try{
            if(is_null($group)){
                $group = Productgroup::create([
                    'manufacturer_id' => $manufacturerId,
                    'ext_id' => $name
                ]);
            }
        }catch (QueryException $e){
            $group = Productgroup::create(['manufacturer_id' => $manufacturerId,'ext_id' => 'rand_'.rand(0,10000000000)]);
        }

        return $group;

    }
}
