<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;


class Category extends Model
{
    use HasFactory;
    use HasTranslations;

    public $guarded = [];
    public $translatable = ['name'];

    public static function findByName($name,$lang){
        $category = Category::where('name->'.$lang,$name)->first();
        if(is_null($category)){
            $category = Category::create([
                'name' => [
                    $lang => $name,
                    'enabled' => 0,
                    'parent_id' => 0,
                    'to_be_checked' => 1
                ]
            ]);
        }
        return $category;
    }

}
