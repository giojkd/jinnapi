<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Spatie\Translatable\HasTranslations;

class Product extends Model
{
    use HasFactory;
    use HasTranslations;
    protected $guarded = [];

    public $translatable = [
        'html_description',
        'name',
        'subtitle',
        'short_description',
        'description',
        'meta_description',
        'meta_title',
        'current_url',
        'advantages',
        'ways_of_use',
        'special_contents',
        'features'
    ];

    public function makeUrl($langs = null){

        $langs = (!is_null($langs)) ? $langs : ['it','en'];
        foreach($langs as $lang){

            $url = [];
            $url[] = $lang;
            $url[] = 'product';
            $url[] = Str::slug($this->getTranslation('name',$lang)).'-'.$this->id;
            $urlImploded = implode('/', $url);

            Url::create([
                'locale' => $lang,
                'is_custom' => 0,
                'url' => $urlImploded,
                'model_id' => $this->id,
                'model_type' => 'App\\Models\\Product'
            ]);

            $this->setTranslation('current_url', $lang, $urlImploded);
            $this->save();

        }
    }

    public function urls()
    {
        return $this->morphMany(Url::class, 'model');
    }

}
